@extends('layouts.app')

@section('content')
<b-container>
    <b-row align-h="center">       
        <b-col cols="8">
            <b-card title="Inicio de sesion">
            <b-alert show>Ingrese datos</b-alert>
                <b-form method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <b-form-group label="Correo electronico">
                        <b-form-input
                            id="email"
                            name="email"
                            type="email"
                            placeholder="Email"
                            value="{{ old('email') }}" 
                            required autofocus>                        
                        </b-form-input>
                    </b-form-group>

                    <b-form-group label="Contraseña">
                        <b-form-input
                            id="password"
                            name="password"
                            type="password"
                            placeholder="Contraseña"
                            value="{{ old('password') }}"
                            required>                        
                        </b-form-input>
                    </b-form-group>

                    <b-form-group >
                        <b-form-checkbox name="remember" {{ old('remember') ? 'checked="true"' : '' }}>
                            Recordar Contraseña
                        </b-form-checkbox>
                    </b-form-group>

                    <b-button type="submit" variant="primary">Ingresar</b-button>
                    <b-button href="{{ route('password.request') }}" variant="link">Olvidaste contraseña?</b-button>
                </b-form>
        </b-col>
    </b-row>
</b-container>
@endsection
